
function Ajax(el)
{
	this.el = el;
	this.xmlhttp = new XMLHttpRequest();
	this.parameters = '';
	this.callbackParameters = [];
	this.callback = null;
}

Ajax.prototype = {

	get:function(url){
	
		this.xmlhttp.open('GET',url,true);
		this.xmlhttp.send(null);
	},

	add:function(name,value){
	
		if(this.parameters.length > 0)
			this.parameters += '&';
		
		this.parameters += name + '=' + encodeURIComponent(value);
	},
	
	post:function(url){
	
		var length = this.parameters.length;
		if(length > 0){
		
			this.xmlhttp.open('POST',url,true);
			this.xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			this.xmlhttp.setRequestHeader("Content-length",length);
			this.xmlhttp.setRequestHeader("Connection","close");
			this.xmlhttp.send(this.parameters);
			
			return true;
		}
		else
			alert('AJAX: No parameters are set! (POST)');
			
		return false;
	},
	
	setStateChange:function(){
	
		var self = this;
		this.xmlhttp.addEventListener('readystatechange',function(e){
		
			if(self.xmlhttp.readyState == 4 && self.xmlhttp.status == 200){
			
				document.getElementById(self.el).innerHTML = self.xmlhttp.responseText;
				if(self.callback){
				
					if(self.callbackParameters.length > 0)
						self.callback.apply(this, self.callbackParameters);
					else
						self.callback.call(this);
				}
			}
		});
	}
};

(function()
{
	var timeoutResize;
	var sortMenu = document.getElementById('sortMenu');
	var mqLandscape = window.matchMedia("(min-width: 960px) and (orientation:landscape)");

	var sortMenuHide = 0;
	window.addEventListener('resize',function(e){
		
		if(timeoutResize)
			window.clearTimeout(timeoutResize);
			
		timeoutResize = setTimeout(function(){
			
			if(mqLandscape.matches){
			
				sortMenuHide = 1;
				if(sortMenu.style.visibility != 'visible')
					sortMenu.style.visibility = 'visible';
			}
			else if(sortMenuHide){
			
				sortMenu.style.visibility = 'hidden';
				sortMenuHide = 0;
			}
		
		}, 5);
		
	});
	
	function formSearchSubmit(e)
	{
		e.preventDefault();
		
		var t = e.target;
		var search = t.search.value;
		window.location = t.action + "/search/" + encodeURIComponent(search);
	}
	
	for(var i = 0;i < document.forms.length;i++)
	{
		var idForm = document.forms[i].id;
		if(idForm){
		
			switch(idForm)
			{
				case 'formSearch':
					document.forms[i].addEventListener("submit",formSearchSubmit);
					break;
			}
		}
	}
	
	document.getElementById('catTitle').addEventListener('click',function(e){

		e.preventDefault();
	
		if(!sortMenu.style.visibility || sortMenu.style.visibility == 'hidden')
			sortMenu.style.visibility = 'visible';
		else
			sortMenu.style.visibility = 'hidden';
	});
	
	var timerVoteContribution;
	var xhrVoteContribution = new Ajax(null);
	
	function getVoteElements(id)
	{
		var id = 'articleContribution_' + id;
		
		var article = document.getElementById(id);
		var voteNav = article.querySelector('.articleVote ul');
		var voteElements = voteNav.getElementsByTagName('li');
		
		return voteElements;
	}
	
	function voteContribution(voteId,positive)
	{
		var voteEls = getVoteElements(voteId);
		if(!voteEls) return false;
		
		voteEls[1].innerHTML = '<i class="ion-loading-c articleVoteIcon"></i>';
		
		if(timerVoteContribution)
			clearTimeout(timerVoteContribution);
			
		timerVoteContribution = setTimeout(function(){
		
			xhrVoteContribution.get(document.location.origin + '/ajax.php?page=vote&id=' + voteId + '&positive=' + positive);
		},300);
		
		return true;
	}
	
	xhrVoteContribution.xmlhttp.addEventListener('readystatechange',function(e){
	
		if(xhrVoteContribution.xmlhttp.readyState == 4){
			
			if(xhrVoteContribution.xmlhttp.status == 200){
			
				var voted = 0;
				var score = 0;
				
				var data = JSON.parse(xhrVoteContribution.xmlhttp.responseText);
				if(data.id > 0){
				
					var voteEls = getVoteElements(data.id);
					
					voted = voteEls[1].getAttribute('data-voted');
					score = voteEls[1].getAttribute('data-vote-score');
					
					score = parseInt(score);
					voted = parseInt(voted);
					
					if(data.success === 1){
					
						var updateVote = (data.positive == 1 ? 1 : -1);
						if(voted === -1 && data.positive === 0) updateVote = 0;
						else if(voted === 1 && data.positive === 1) updateVote = 0;
					
						score += data.score;
						
						voteEls[0].className = '';
						voteEls[2].className = '';
						
						if(updateVote !== 0){
						
							if(data.positive == 1) voteEls[0].className = 'votePositive';
							else voteEls[2].className = 'voteNegative';
						}
						
						voteEls[1].setAttribute('data-voted',updateVote);
						voteEls[1].setAttribute('data-vote-score',score);
					}
				}
				
				voteEls[1].innerHTML = '<i class="ion-record articleVoteIcon"></i>';
				if(score !== 0)	
					voteEls[1].innerHTML = score;
			
			}
			else if(xhrVoteContribution.xmlhttp.status == 403)
				setTimeout(function(){ window.location = document.location.origin + '/page/register' },300);
		}
	});
	
	var contentPosts = document.getElementById('content_posts');
	if(contentPosts){
	
		contentPosts.addEventListener('click',function(e){
		
			var t = e.target;
			if(t.nodeName === 'I' || t.nodeName === 'A'){
			
				if(t.parentNode.nodeName === 'A')
					t = t.parentNode;
				
				var voteId = t.getAttribute('data-vote');
				if(voteId){
				
					e.preventDefault();
					
					var positive = (t.getAttribute('data-vote-positive') == 1 ? 1 : 0);
					voteContribution(voteId,positive);
				}
			}
		});
	}
	
}());

//-----------------------------------------------------------------------------------------------------------------------------------
