<?php 

	class Contribution extends Main
	{
		private function __clone(){}
		function __construct(){parent::__construct();}
		
		public function getMany($page = 1,$search = '', $category = 0, $sort = 'new', $voter = 0)
		{
			$options = [
			
				'sort' => $sort, 
				'search' => $search, 
				'category' => $category,
				'voter' => $voter
			];
			
			$maxRows = 15;
			
			return $this->dbHelper->selectComplexContributions($page,$options,$maxRows);
		}
		
		public function create(&$user,$title,$url,$categoryName,$secureimage)
		{
			$created = false;
		
			$errors_title = getLengthError($title,'title');
			if(!empty($errors_title)){
			
				$this->setErrorMessages($errors_title,'title');
				return false;
			}
		
			$errors_url = vURL($url);
			if(!empty($errors_url)){
			
				$this->setErrorMessages($errors_url,'url');
				return false;
			}
			
			if($categoryName)
				$category = $this->dbHelper->selectCategoryByName($categoryName,'`id`,`contributions`');
			else
				$category = $this->dbHelper->selectCategoryById(DEFAULT_CATEGORY_ID,'`id`,`contributions`');
			
			if(!$category){
			
				$this->setErrorMessages(_translate('TXT_CATEGORY_NONE'),'category','name');
				return false;
			}
			
			require_once __DIR__ . '/../securimage/securimage.php';
			$securimage = new Securimage();
			$securimageValid = $securimage->check($secureimage);
			if(!$securimageValid)
			{
				$this->setErrorMessages(_translate('TXT_INVALID_CAPTCHA'),'captcha');
				return false;
			}
			
			$this->dbHelper->beginTransaction();
			$user->setContributions($user->contributions() + 1);
			if($user->save())
			{
				$categoryUpdated = $this->dbHelper->updateCategoryContributions($category['id'],$category['contributions'] + 1);
				if($categoryUpdated && $this->dbHelper->updateAddRows(SQL_DB_CONTRIBUTIONS,1)){
				
					if($this->dbHelper->insertContribution($category['id'],$user->id(),$title,$url)){ 
					
						$id = $this->dbHelper->lastInsertId();
						$this->dbHelper->commit();
						return ['id' => $id, 'cat_id' => $category['id'], 'cat_name' => $category['name']];
					}
				}
			}
			
			$this->dbHelper->rollBack();
			return false;
		}
	}