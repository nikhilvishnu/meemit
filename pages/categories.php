<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = 'Submeemits';
	$g_htmlDescription 	.= ' '. _translate('HTML_DESCRIPTION_CATEGORIES');
	
	$pageLink = ifsetor($_REQUEST['page_link']);
	$search = ifsetor($_REQUEST['search'],'');
	
	$arrCategories = $g_category->getMany($pageLink,$search);
	$pageVars = $arrCategories[0];
	$articles = $arrCategories[1];
	
	$searched = '';
	if($search)
		$searched = '/search/'.rawurlencode($search);
	$pageURL = $g_hostURL.'/page/categories'.$searched.'&amp;page_link=';
	
	$output_pageLinks = pageLinks($pageURL,$pageVars['page_numbers'],$pageVars['page_numbers'],$pageVars['page']);
	
