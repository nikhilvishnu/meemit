<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('DELETE_CONTRIBUTION');
	
	$id = ifsetor($_GET['id']);
	
	$clientUser->deleteContribution($id);
	headerRedirect($g_previousURL);
	