<?php 

	if(!IN_SITE)
	{
		exit;
	}
	
	class Configs
    {
        const DBEXT_PDO 			= 1;
		const DBEXT_MYSQLI 			= 2;
		
		const OID_RETURN_DOMAIN 	= 'www.meemit.xuniver.se';
		const OID_P_GOOGLE 			= 'www.google.com';
		
		private static $dbExt;
		
		//Only add providers that support OpenID Attribute Exchange and contact/email.
		public static $openidProviders = [
			
			self::OID_P_GOOGLE => 'https://www.google.com/accounts/o8/id'
			
		];
		
		static public function getDatabaseExtension()
		{
			switch(DB_TYPE)
			{
				case 'mysqli':
					static::$dbExt = self::DBEXT_MYSQLI;
					break;
				default:
					static::$dbExt = self::DBEXT_PDO;
			}
			
			return static::$dbExt;
		}
		
		private function __construct(){}
		private function __clone(){}
    }
	
	