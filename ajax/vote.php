<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$id = ifsetor($_GET['id']);
	$positive = ifsetor($_GET['positive']);
	$positive = (int) $positive;
	$score = $clientUser->voteContribution($id,$positive);

	header('Content-Type: application/json; charset=utf-8');

	$success = 0;
	if($score !== false)
		$success = 1;
	
	$id = ($success) ? $id : 0;
	
	printf('{ "id": %d, "success": %d, "score": %d, "positive": %d }', $id, $success, $score, $positive);

?>