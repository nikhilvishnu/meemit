<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div class='subContentContainer'>
	<div id='content_post' class='subContainer'>
		<h2><?=$g_title?></h2>
		<form id='formPost' action="<?=$g_hostURL?>/page/contribute" method='POST'>
			<fieldset class='subContent'>
				<legend><?=_translate('QSN_SHARE')?></legend>
				<p class='formDescription'><?=_translate('TXT_DESC_POST')?></p>
				<div>
					<label for='inputTitle'><?=_translate('TITLE')?></label>
					<p class='inputDescription<?=$classTitle?>'><?=$descTitle?></p>
					<input name="title" id="inputTitle" pattern='.{<?=LENGTH_MIN_TITLE?>,}' value='<?=$title?>' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_TITLE)?>' maxlength="<?=LENGTH_MAX_TITLE?>" required>
				</div>
				<div>
					<label for='inputURL'><?=_translate('URL','UC')?></label>
					<p class='inputDescription<?=$classURL?>'><?=$descURL?></p>
					<input type="url" name="url" id="inputURL" pattern='.{<?=LENGTH_MIN_URL?>,}' value='<?=$url?>' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_URL)?>' maxlength="<?=LENGTH_MAX_URL?>" required>
				</div>
				<div>
					<label for='inputCategory'>Submeemit</label>
					<p class='inputDescription<?=$classCategory?>'><?=$descCategory?></p>
					<input name="category" id='inputCategory' value='<?=$category?>'> <span class='inputOptional'>(<?=_translate('OPTIONAL')?>)</span>
				</div>
				
				
				<div class='si_captcha'>
					<label for='inputCaptcha'><?=_translate('CAPTCHA')?></label>
					<img id="siimage" class="si_image" 
						src="<?=$g_hostURL?>/securimage/securimage_show.php?sid=<?=md5(uniqid())?>" alt="CAPTCHA Image"> 
				</div>
				
				<div>
					<div class='si_option'>
						<object type="application/x-shockwave-flash" class="si_audio" 
							data="<?=$g_hostURL?>/securimage/securimage_play.swf?icon_file=<?=$g_hostURL?>/securimage/images/audio_icon.png&amp;audio_file=<?=$g_hostURL?>/securimage/securimage_play.php">
							
							
							<param name="movie" value="<?=$g_hostURL?>/securimage/securimage_play.swf?icon_file=<?=$g_hostURL?>/securimage/images/audio_icon.png&amp;audio_file=<?=$g_hostURL?>/securimage/securimage_play.php">
						</object>
					</div>
					 
					<div class='si_option'>
						<a tabindex="-1" href="#" title="Refresh Image" 
							onclick="document.getElementById('siimage').src = '<?=$g_hostURL?>/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false">
								<img src="<?=$g_hostURL?>/securimage/images/refresh.png" class='si_refresh' alt="Reload Image">
						</a>
					</div>
				</div>
				
				<div class='si_captchaInput'>
					<label for='inputCaptcha'><?=_translate('ENTER_CODE')?></label>
					<p class='inputDescription<?=$classCaptcha?>'><?=$descCaptcha?></p>
					<input name="captcha" maxlength="16" placeholder='<?=_translate('TXT_CAPTCHA_PLACEHOLDER')?>' id="inputCaptcha" required>
				</div>
				
				<div>
					<button type='submit' name='submitContribution' value='<?=_translate('CONTRIBUTE')?>'><?=_translate('CONTRIBUTE')?></button>
				</div>
			</fieldset>
		</form>
	</div>
</div>