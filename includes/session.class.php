<?php 

	class Session extends Main
	{
		protected $user;
	
		function __construct(){parent::__construct();}
		
		private function check()
		{
			$hashed_userAgent 	= hash('md5',$_SERVER['HTTP_USER_AGENT']);
			$hashed_remoteIP 	= hash('md5',getIP());
			
			if(empty($_SESSION[SESS_KEY_H_HTTPUSERAGENT]) || empty($_SESSION[SESS_KEY_H_REMOTEADDR])){
			
				$_SESSION[SESS_KEY_H_HTTPUSERAGENT] = $hashed_userAgent;
				$_SESSION[SESS_KEY_H_REMOTEADDR]	= $hashed_remoteIP;
			}
			
			if($_SESSION[SESS_KEY_H_HTTPUSERAGENT] !== $hashed_userAgent || $_SESSION[SESS_KEY_H_REMOTEADDR] !== $hashed_remoteIP){
			
				$this->destroy();
				return false;
			}
			
			return true;
		}
	
		public function setUser($id)
		{
			if(!$id) return false;
		
			session_regenerate_id(true);
			if($this->dbHelper->updateLogin($id) !== false){
			
				$_SESSION[SESS_KEY_USERID] = $id;
				return true;
			}
			
			return false;
		}
		
		public function destroy()
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]);
			
			session_unset();
			session_destroy();
			
			return true;
		}
	
		public function checkAuthorized()
		{
			$user = null;
			$userid = ifSetOr($_SESSION[SESS_KEY_USERID]);
			if(!$userid)
				return false;
			
			if($userid && $this->check()){
			
				$this->user = new User($userid);
				if($this->user->id() > 0){
				
					if($_SESSION[SESS_KEY_H_REMOTEADDR] ===  hash('md5',$this->user->ip()) && session_id() === $this->user->id_session()){
					
						$_SESSION[SESS_KEY_AUTHORIZED] = 1;
						$this->user->setOnline();
						
						return $this->user;
					}
					else
					{
						$this->dbHelper->updateDBSessionId($this->user->id(),'0');
						$this->destroy();
					}
				}
			}

			return false;
		}
	}