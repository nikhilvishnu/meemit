<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}

	class Site extends Main
	{
		private function __clone(){}
		function __construct(){parent::__construct();}
		
		public function createUser($email,$openid,$password = '',$passwordConfirmation = '', $secureimage = '')
		{
			$errors_email = vEmail($email);
			if(!empty($errors_email)){
			
				$this->setErrorMessages($errors_email,'email');
				return false;
			}
			
			$created = false;
			if($openid){
			
				$this->dbHelper->beginTransaction();
				if($this->dbHelper->updateAddRows(SQL_DB_USERS,1) && $this->dbHelper->insertUser($email,true)){
				
					$id = $this->dbHelper->lastInsertId();
					if($id > 0){
					
						$provider = parse_url($openid)['host'];
						
						$openid = hash_openid($openid,$email);
						if($this->dbHelper->updateAddRows(SQL_DB_USERS_OPENIDS,1) && $this->dbHelper->insertOpenId($id,$openid,$provider)){
							
							$this->dbHelper->commit();
							return $id;
						}
					}
				}
				
				$this->dbHelper->rollBack();
			}
			else{
			
				$errors_password = vPassword($password,$passwordConfirmation);
				if(!empty($errors_password)){
				
					$this->setErrorMessages($errors_password,'password');
					return false;
				}

				$user = $this->dbHelper->selectUserByEmail($email,'`id`');
				if($user && $user['id'] > 0){
				
					$this->setErrorMessages(_translate('TXT_EMAIL_EXISTS'),'email');
					return false;
				}
				
				require_once __DIR__ . '/../securimage/securimage.php';
				$securimage = new Securimage();
				$securimageValid = $securimage->check($secureimage);
				if(!$securimageValid)
				{
					$this->setErrorMessages(_translate('TXT_INVALID_CAPTCHA'),'captcha');
					return false;
				}
			
				$password = hash_password($password,$email);
				
				$this->dbHelper->beginTransaction();
				if($this->dbHelper->insertUser($email,false,$password)){
				
					$id = $this->dbHelper->lastInsertId();
					$this->dbHelper->commit();
					return $id;
				}
				
				$this->dbHelper->rollBack();
			}
			
			return false;
		}
		
		public function authenticateUser($login,$identity,$password = '')
		{
			$id = 0;
			$authenticated = false;
			
			if($identity){
			
				if(!$login) return false;
				
				$user = $this->dbHelper->selectUserByEmail($login,'`id`,`email`');
				if($user && $user['id'] > 0){
				
					$identity = hash_openid($identity,$user['email']);
					$openid = $this->dbHelper->selectOpenIdUser($user['id'],$identity,'`id`');
					if($openid){
					
						$id = $user['id'];
						$authenticated = true;
					}
					else
						$this->setErrorMessages(_translate('TXT_OPENID_NOLINK'));
					
				}
				else
				{
					$created = $this->createUser($login,$identity);
					if($created){
					
						$id = $created;
						$authenticated = true;
					}
				}
			}
			else
			{
				if(!$password || !$login) return false;
				
				$user = $this->dbHelper->selectUserByEmailUsername($login,$login,true,'`id`,`email`,`password`');
				if($user && $user['id'] > 0){
				
					if(password_verify(getPassword($password,$user['email']),$user['password'])){
					
						$id = $user['id'];
						$authenticated = true;
					}
				}
				
				if(!$authenticated)
					$this->setErrorMessages(_translate('TXT_INVALID_LOGIN'));
			}
			
			if($authenticated)
			{
				$session = new Session();
				return $session->setUser($id);
			}
				
			return false;
		}
	}

