<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div id='content_posts' class='responsiveContainer'>
	<h2><?=_translate('CONTRIBUTIONS')?></h2>
	<p><?=_translate('TXT_USER_CONTRIBUTIONS')?></p>
	<?php require __DIR__ . '/includes/pagelinks.php'; ?>
	<div class='articleContainer'>
	
		<?php require __DIR__ . '/includes/contributions.php'; ?>
		
	</div>
	<?php require __DIR__ . '/includes/pagelinks.php'; ?>
	<?php include __DIR__ . '/includes/ads_footer.php'; ?>
</div>

<div id='content_profile' class='sidePanel'>
	<form id='formSearch' action="<?=$g_sortedURL?>" method='POST'>
		<div>
			<input type='search' placeholder='Meemits' name="search" id="inputSearch" /> 
		</div>
		<div>
			<button type='submit' name='submitSearch' value='Search'><?=_translate('SEARCH')?></button>
		</div>
	</form>
	<div id='profile'>
		<h2><?=$profilename?></h2>
		<span id='profileUserStatus' class='<?=$classOnline?>'><?=$descOnline?></span>
		<table id='profileInformation'>
			<caption><?=_translate('INFORMATION')?></caption>
			<tbody>
				<tr>
					<td><?=_translate('FIRSTNAME')?>: </td>
					<td><?=$firstname?></td>
				</tr>
				<tr>
					<td><?=_translate('LASTNAME')?>: </td>
					<td><?=$lastname?></td>
				</tr>
			</tbody>
		</table>
		<table id='profileStats'>
			<caption><?=_translate('STATISTICS')?></caption>
			<tbody>
				<tr>
					<td><?=_translate('CONTRIBUTED')?>: </td>
					<td><?=$contributions?></td>
				</tr>
				<tr>
					<td><?=_translate('KARMA_CONTRIBUTION')?>: </td>
					<td><?=$karma_contribution?></td>
				</tr>
				<tr>
					<td><?=_translate('VOTES_CONTRIBUTION')?>: </td>
					<td><?=$votes_contribution?></td>
				</tr>
			</tbody>
		</table>
		
		<?php if($description): ?>

		<p title='<?=_translate('DESCRIPTION')?>' class='descriptionContent'>
			<?=$description?>
		</p>
		
		<?php endif; ?>
		
		<?php include __DIR__ . '/includes/ads_sidebar.php'; ?>
		
	</div>
</div>

