<?php 

	define('IN_SITE',true);
	
	require __DIR__ . '/includes/main.php';
	setSpamFilter('ajax');
	
	$g_getPage = ifSetOr($_GET['page'],'unauthorized');
	$g_getPage = pageAjax($g_getPage);
	
	require __DIR__ . '/ajax/'.$g_getPage.'.php';
	
?>
