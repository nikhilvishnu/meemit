<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('TIT_LOGIN_REG');
	$g_htmlDescription 	.= ' '. _translate('HTML_DESCRIPTION_REGISTER');
	
	$email = '';
	$created = false;
	
	$descEmail 		= '';
	$descPassword 	= _translate('TXT_VALID_PASSWORD');
	$descCaptcha 	= '';
	
	$classEmail		= '';
	$classPassword	= '';
	$classCaptcha	= '';
	
	$submitRegister = ifsetor($_POST['submitRegister']);
	if($submitRegister){
	
		$email = ifsetor($_POST['email']);
		$password = ifsetor($_POST['password']);
		$password2 = ifsetor($_POST['password2']);
		$captcha = ifsetor($_POST['captcha']);
		
		$created = $site->createUser($email,false,$password,$password2,$captcha);
		if($session->setUser($created))
			headerRedirect($g_hostURL);
		else
		{
			$error = $site->getFirstError('email');
			if($error) $classEmail = ' failure';
			$descEmail = ($error) ? $error : $descEmail;
			
			$error = $site->getFirstError('password');
			if($error) $classPassword = ' failure';
			$descPassword = ($error) ? $error : $descPassword;
			
			$error = $site->getFirstError('captcha');
			if($error) $classCaptcha = ' failure';
			$descCaptcha = ($error) ? $error : $descCaptcha;
		}
		
		$email = sanitizeSpChars($email);
	}
	
