
CREATE TABLE `comments`(
	`id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
	`id_contribution` BIGINT unsigned NOT NULL,
	`id_user` BIGINT unsigned NOT NULL,
	`id_parent` BIGINT unsigned NOT NULL,
	`content` varchar(255) NOT NULL,
	`score_positive` INT unsigned DEFAULT 0,
	`score_negative` INT unsigned DEFAULT 0,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	INDEX (`id_contribution`),
	FOREIGN KEY (`id_contribution`)
        REFERENCES `contributions`(`id`) ON DELETE CASCADE,
	INDEX (`id_user`),
	FOREIGN KEY (`id_user`)
        REFERENCES `users`(`id`) ON DELETE CASCADE,
	INDEX (`id_parent`),
	FOREIGN KEY (`id_parent`)
        REFERENCES `comments`(`id`) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8;

CREATE TABLE `comments_ratings`(
	`id` BIGINT unsigned NOT NULL AUTO_INCREMENT,
	`id_comment` BIGINT unsigned NOT NULL,
	`id_user` BIGINT unsigned NOT NULL,
	`voted` TINYINT DEFAULT NULL,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`),
	INDEX (`id_comment`),
	FOREIGN KEY (`id_comment`)
        REFERENCES `comments`(`id`) ON DELETE CASCADE,
	INDEX (`id_user`),
	FOREIGN KEY (`id_user`)
        REFERENCES `users`(`id`) ON DELETE CASCADE,
	UNIQUE `comment_users`(`id_comment`,`id_user`)
) DEFAULT CHARACTER SET utf8;

