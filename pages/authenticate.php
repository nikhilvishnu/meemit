<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('AUTHENTICATE');

	$openidProvider = ifsetor($_GET['openid']);
	$submitLogin = ifsetor($_POST['submitLogin']);
	
	//To do: Change to Google API
	//Doesn't work with android phones, URL length is too long.
	$authenticated = false;
	if($openidProvider){
	
		$openidURL = ifsetor(Configs::$openidProviders[$openidProvider]);
		if($openidURL){
		
			require __DIR__ . '/../includes/thirdparties/lightopenid/openid.php';
			
			try
			{
				$openid = new LightOpenID(Configs::OID_RETURN_DOMAIN);
				if(!$openid->mode){
				
					if(isset($_GET['login'])){
					
						$openid->identity = $openidURL;
						$openid->required = array('contact/email');

						$openid_token = uniqid();
						$_SESSION[SESS_KEY_OPENID_TOKEN] = $openid_token;
						$openid->returnUrl = $g_hostURL.'/index.php?page=authenticate&openid='.rawurlencode($openidProvider).'&token='.$openid_token;
						
						headerRedirect($openid->authUrl());
					}
				}
				elseif($openid->mode == 'cancel'){
				
					unset($_SESSION[SESS_KEY_OPENID_TOKEN]);
					headerRedirect($g_hostURL);
				}
				else
				{
					$openid_token = ifsetor($_GET['token']);
					$openid_validToken = ifsetor($_SESSION[SESS_KEY_OPENID_TOKEN]);
					
					if(($openid_token === $openid_validToken) && ($openid->validate())){
					
						$user = $openid->getAttributes();
						if($user){
						
							$user['openid/identity'] = $openid->identity;
							$_SESSION[SESS_KEY_OPENID] = $user;
						}
					}
					
					unset($_SESSION[SESS_KEY_OPENID_TOKEN]);
					headerRedirect($g_hostURL . '/page/authenticate/openid/'.rawurlencode($openidProvider));
				}
			}
			catch(ErrorException $e){}
			
			if(isset($_SESSION[SESS_KEY_OPENID])){
			
				$user = $_SESSION[SESS_KEY_OPENID];
				$identity = ifsetor($user['openid/identity']);
				$email = ifsetor($user['contact/email']);
				
				unset($_SESSION[SESS_KEY_OPENID]);
				$authenticated = $site->authenticateUser($email,$identity);
			}
		}
		else
		{
			$site->setErrorMessages(_translate('TXT_INVALID_OPENID'));
		}
	}
	else
	{
		$login = '';
		$password = '';
		if($submitLogin){
			
			$login = ifsetor($_POST['login'],'');
			$password = ifsetor($_POST['password'],'');
			$authenticated = $site->authenticateUser($login,false,$password);
		}
	}

	if($authenticated || (!$openidProvider && !$submitLogin))
		headerRedirect($g_hostURL);

	$errors = $site->getFirstError();

	