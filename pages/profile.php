<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('PROFILE');
	
	$id = ifsetor($_REQUEST['id']);
	$user = new User($id);
	if($user->id() < 1)
		headerRedirect($g_hostURL);
		
	$profilename = $user->getProfileName();
	$g_title = $profilename.' - '.$g_title;
	$g_htmlDescription 	.= ' '. sprintf(_translate('HTML_DESCRIPTION_PROFILE'),$profilename);
	
	$firstname = _translate('UNKNOWN');
	if($user->show_firstname() && $user->firstname())
		$firstname = sanitizeSpChars($user->firstname());
	
	$lastname = _translate('UNKNOWN');
	if($user->show_lastname() && $user->lastname())
		$lastname = sanitizeSpChars($user->lastname());
	
	$description = '';
	if($user->description()){
		$description = $user->description();
		$description = sanitizeSpChars($description);
		$description = nl2br($description);
	}
	
	$online = $user->online();
	
	$descOnline 	= ' '._translate('OFFLINE').' <i class="ion-record"></i>';
	$classOnline		= 'offline';
	if(activeSince($online)){
	
		$descOnline 	= ' '._translate('ONLINE').' <i class="ion-record"></i>';
		$classOnline		= 'online';
	}
	
	$g_sortURL = $g_hostURL.'/page/'.$user->getProfileLink();
	$g_sortedURL = $g_sortURL.$g_sortedURL;
	
	$karma_contribution = $user->karma_contribution();
	$votes_contribution = $user->votes_contribution();
	$contributions = $user->contributions();
	
	$pageLink = ifsetor($_REQUEST['page_link']);
	$search = ifsetor($_REQUEST['search'],'');
	
	$arrContributions = $user->getContributions($pageLink,$search,$g_getSort,$g_userId);
	$pageVars = $arrContributions[0];
	$articles = $arrContributions[1];
	
	$searched = '';
	if($search)
		$searched = '/search/'.rawurlencode($search);
	$pageURL = $g_sortedURL.$searched.'&amp;page_link=';
	
	$output_pageLinks = pageLinks($pageURL,$pageVars['page_numbers'],$pageVars['page_numbers'],$pageVars['page']);
	
	