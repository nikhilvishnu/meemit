<?php
	
	if(!IN_SITE)
	{
		exit;
	}
	
	require_once __DIR__ . '/database-helper.class.php';
	require_once __DIR__ . '/database-pdo.class.php';
	require_once __DIR__ . '/database-mysqli.class.php';
	
    class DatabaseFactory
    {
		static private $instances = [];
		static private $index = '';

		private function __construct(){}
		private function __clone(){}

		static public function create($type,$index = 'master',$host = '',$port = '',$password = '',$user = '',$database = '',$charset = '',$databaseType = '')
		{
			if(empty(static::$instances[$index]))
			{
	            switch($type)
	            {
	                case Configs::DBEXT_PDO:
						 static::$instances[$index] = new DatabaseHandlerPDO();
						break;
	                case Configs::DBEXT_MYSQLI:
						static::$instances[$index] = new DatabaseHandlerMySQLi();
						break;
	            }
				
				end(static::$instances);
				static::$index = key(static::$instances);
				
				if(static::$instances[static::$index])
				{
					static::$instances[static::$index]->config($host,$port,$password,$user,$database,$charset,$databaseType);
					static::$instances[static::$index]->connect();
				}
			}
			else
			{
				static::$index = $index;
			}
		}

		static public function getIndex(){return static::$index;}
		
		static public function getIndexes(){return array_keys(static::$instances);}
		
		static public function getInstance($index = null)
		{
			if(!$index) $index = static::$index;
		
			if(array_key_exists($index,static::$instances))
				return static::$instances[$index];
				
			return false;
		}
		
		static public function closeInstance($index = null)
		{
			if(!$index) $index = $this->index;
		
			if(array_key_exists($index,static::$instances))
			{
				unset(static::$instances[$index]);
				return true;
			}
				
			return false;
		}
    }
	
	class DatabaseHandler
	{
		protected $handler = null;
		protected $connected = false;
		
		protected $errno = 0;
		protected $error = '';
	
		private $type 		= '';
		private $host 		= '';
		private $port 		= '';
		private $database	= '';
		private $charset	= '';
		private $user		= '';
		private $password 	= '';
		
		public function config($host,$port,$password,$user,$database,$charset,$type)
		{
			$this->type 		= ($type 		!= '')		? $type 	: DB_TYPE;
			$this->host  		= ($host 		!= '') 		? $host 	: DB_HOST;
			$this->port  		= ($port 		!= '') 		? $port 	: DB_PORT;
			$this->database		= ($database 	!= '') 		? $database : DB_DATABASE;
			$this->charset		= ($charset 	!= '') 		? $charset 	: DB_CHARSET;
			$this->user			= ($user 		!= '') 		? $user 	: DB_USER;
			$this->password		= ($password 	!= '') 		? $password	: DB_PASSWORD;
		}
		
		protected function type(){return $this->type;}
		protected function host(){return $this->host;}
		protected function port(){return $this->port;}
		protected function database(){return $this->database;}
		protected function charset(){return $this->charset;}
		protected function user(){return $this->user;}
		protected function password(){return $this->password;}
		
		public function outputError()
		{
			if($this->errno > 0)
			{
				if(DB_PRINT_ERROR)
				{
					$message = sprintf(_translate('TXT_SQL_ERROR'),$this->errno,$this->error);
					return $message;
				}
				
				return _translate('TXT_SQL_PUBLIC_ERROR');
			}
				
			return "";
		}
		
		public function resetError()
		{
			$this->errno = 0;
			$this->error = '';
			
			return true;
		}
		
		public function handler(){return $this->handler;}
		public function connected(){return $this->connected;}
	}

	abstract class DatabaseLayer extends DatabaseHandler
	{
		protected function getDataType($value)
		{
			if(is_int($value))
				return 'i';
				
			if(is_string($value))
				return 's';
				
			if(is_float($value))
				return 'd';
				
			if(is_bool($value))
				return 'b';
			
			//is_binary?
			if(@bin2hex($value))
				return 'l';
		
			return false;
		}
	
		public function fetchRowById($table,$id,$select = '`id`')
		{
			$id = (int) $id;
			if($id < 1)
				return false;
		
			return $this->fetchRowByColumn($table,'id',$id,$select);
		}
		
		public function deleteRowById($table,$id)
		{
			$id = (int) $id;
			if($id < 1)
				return false;
				
			return $this->deleteRowByColumn($table,'id',$id);
		}
		
		public function fetchRowByColumn($table,$column,$value,$select = '*')
		{
			$sql 	= 'SELECT '.$select.' FROM `'.$table.'` WHERE `'.$column.'`=? LIMIT 1';
			$sql_params = [[$value,$this->getDataType($value)]];
			
			$row = $this->fetchAssoc($sql,$sql_params);
			if($row)
				return $row;
				
			return false;
		}
		
		public function deleteRowByColumn($table,$column,$value)
		{
			$sql = 'DELETE FROM `'.$table.'` WHERE `'.$column.'`=? LIMIT 1';
			$sql_params = [[$value,$this->getDataType($value)]];
			
			$executed = $this->executeQuery($sql,$sql_params);
			if($executed)
			{
				return true;
			}
		
			return false;
		}
		
		abstract public function connect();
		
		abstract public function prepare($sql);
		abstract public function beginTransaction();
		abstract public function lastInsertId();
		abstract public function commit();
		abstract public function rollBack();
		abstract public function bindParam($stmt,$i,&$value,$type = 's');
		abstract public function closeStatement($stmt);
		abstract public function execute($stmt);
		abstract public function setLastError($stmt = null);
		
		abstract public function bindArray($stmt,array &$parameters);
		abstract public function fetchAssoc($sql,array $params = [],$all = false);
		abstract public function executeQuery($sql,array $params = []);
		
		abstract protected function dataType($type);
	}
	
