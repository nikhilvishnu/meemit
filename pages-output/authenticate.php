<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div class='subContentContainer'>
	<div id='content_authenticate' class='subContainer'>
		<h2><?=_translate('AUTHENTICATE')?></h2>
		<?php if($errors): ?>
		
		<p class='failureContent'>
			<?=$errors?>
		</p>
		
		<?php endif; ?>
		
		<?php require __DIR__ . '/includes/login.php'; ?>
		
	</div>
</div>