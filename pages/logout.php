<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}

	$g_title = 'Logout';
	$session->destroy();
	headerRedirect($g_hostURL);