<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('OPENID');

	$openidProvider = ifsetor($_GET['provider']);
	$updatedOpenId = false;
	
	$openidURL = ifsetor(Configs::$openidProviders[$openidProvider]);
	if($openidURL){
	
		require __DIR__ . '/../includes/thirdparties/lightopenid/openid.php';
		
		try
		{
			$openid = new LightOpenID(Configs::OID_RETURN_DOMAIN);
			if(!$openid->mode){
			
				if(isset($_GET['login'])){
				
					$openid->identity = $openidURL;
					$openid->required = array('contact/email');

					$openid_token = uniqid();
					$_SESSION[SESS_KEY_OPENID_TOKEN] = $openid_token;
					$openid->returnUrl = $g_hostURL.'/index.php?page=openid&provider='.rawurlencode($openidProvider).'&token='.$openid_token;
					
					headerRedirect($openid->authUrl());
				}
			}
			elseif($openid->mode == 'cancel'){
			
				unset($_SESSION[SESS_KEY_OPENID_TOKEN]);
				headerRedirect($g_hostURL. '/page/settings');
			}
			else
			{
				$openid_token = ifsetor($_GET['token']);
				$openid_validToken = ifsetor($_SESSION[SESS_KEY_OPENID_TOKEN]);
				
				if(($openid_token === $openid_validToken) && ($openid->validate())){
				
					$user = $openid->getAttributes();
					if($user){
					
						$user['openid/identity'] = $openid->identity;
						$_SESSION[SESS_KEY_OPENID] = $user;
					}
				}
				
				unset($_SESSION[SESS_KEY_OPENID_TOKEN]);
				headerRedirect($g_hostURL . '/page/openid/provider/'.rawurlencode($openidProvider));
			}
		}
		catch(ErrorException $e){}
		
		if(isset($_SESSION[SESS_KEY_OPENID])){
		
			$user = $_SESSION[SESS_KEY_OPENID];
			$identity = ifsetor($user['openid/identity']);
			$email = ifsetor($user['contact/email']);
			
			unset($_SESSION[SESS_KEY_OPENID]);
			$updatedOpenId = $clientUser->addRemoveOpenId($identity,$email);
		}
		else
		{
			headerRedirect($g_hostURL. '/page/settings');
		}
	}
	else
	{
		$site->setErrorMessages(_translate('TXT_INVALID_OPENID'));
	}
	
	if($updatedOpenId || !$openidProvider)
		headerRedirect($g_hostURL. '/page/settings');
	
	$errors = [];
	if($site->getFirstError())
		$errors[] = $site->getFirstError();
		
	if($clientUser->getFirstError('openid'))
		$errors[] = $clientUser->getFirstError('openid');
	
