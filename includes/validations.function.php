<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	function vTextContent($str,$pattern){
	
		$pattern = ifsetor($pattern,'');
		switch($pattern)
		{
			case "username"		: 	$pattern = "/^(\p{L}|\p{N})+([-_]{1}(\p{L}|\p{N})|(\p{L}|\p{N}))+$/iu";
				break;
			case "name"			: 	$pattern = "/^(\p{L})+(([\.\s-]{1})(\p{L})|(\p{L}))+$/iu";
				break;
			case "numbers"		: 	$pattern = "/\p{N}/u";
				break;
			case "letters"		: 	$pattern = "/\p{L}/iu";
				break;
			case "uppercase"	: 	$pattern = "/\p{Lu}/u";
				break;
			case "lowercase"	: 	$pattern = "/\p{Ll}/u";
				break;
			case "symbols"		: 	$pattern = "/\p{S}/u";
				break;
		}
		
		if(preg_match($pattern,$str))
			return true;
		
		return false;
	}

	function vTextLength($str,$maxLength,$minLength = 1){
	
		$str = (string) $str;
		$length = mb_strlen($str);
		
		if($length < $minLength)
			return 1;
		else if($length > $maxLength)
			return 2;

		return 0;
	}
	
	function getLengthError($str,$input)
	{
		$errors = [];
		$input = ifsetor($input);
		
		switch($input)
		{
			case 'firstname':
				$length_max = LENGTH_MAX_NAME;
				$length_min = LENGTH_MIN_NAME;
				$inputname	= 'FIRSTNAME';
				break;
			case 'lastname':
				$length_max = LENGTH_MAX_NAME;
				$length_min = LENGTH_MIN_NAME;
				$inputname	= 'LASTNAME';
				break;
			case 'username':
				$length_max = LENGTH_MAX_USERNAME;
				$length_min = LENGTH_MIN_USERNAME;
				$inputname	= 'USERNAME';
				break;
			case 'password':
				$length_max = LENGTH_MAX_PASSWORD;
				$length_min = LENGTH_MIN_PASSWORD;
				$inputname	= 'PASSWORD';
				break;
			case 'email':
				$length_max = LENGTH_MAX_EMAIL;
				$length_min = LENGTH_MIN_EMAIL;
				$inputname	= 'EMAIL';
				break;
			case 'title':
				$length_max = LENGTH_MAX_TITLE;
				$length_min = LENGTH_MIN_TITLE;
				$inputname	= 'TITLE';
				break;
			case 'url':
				$length_max = LENGTH_MAX_URL;
				$length_min = LENGTH_MIN_URL;
				$inputname	= 'URL';
				break;
			case 'description':
				$length_max = LENGTH_MAX_DESCRIPTION;
				$length_min = LENGTH_MIN_DESCRIPTION;
				$inputname	= 'DESCRIPTION';
				break;
			default:
				$input = '';
		}
		
		if($input){
		
			$errors_length = vTextLength($str,$length_max,$length_min);
			if($errors_length == 1) $errors[] = sprintf(_translate('TXT_INPUT_TOO_SHORT'),_translate($inputname,'LC'),$length_min);
			else if($errors_length == 2) $errors[] = sprintf(_translate('TXT_INPUT_TOO_LONG'),_translate($inputname,'LC'),$length_max);
			
			return $errors;
		}
		
		$errors[] = sprintf(_translate('TXT_CODING_ERROR'),'getLengthError()');
		return $errors;
	}
	
	function vPassword($password,$confirmation,$strenght = 2)
	{
		$errors = getLengthError($password,'password');
		if(empty($errors)){
			
			if($strenght > 0 && !vTextContent($password,'numbers'))
			{
				$errors[] = _translate('TXT_PASSWORD_INCLUDE_NUM');
			}
			
			if($strenght > 1 && !vTextContent($password,'letters'))
			{
				$errors[] = _translate('TXT_PASSWORD_INCLUDE_L');
			}
			
			if($strenght > 2 && !vTextContent($password,'uppercase'))
			{
				$errors[] = _translate('TXT_PASSWORD_INCLUDE_UCL');
			}
			
			if($strenght > 3 && !vTextContent($password,'lowercase'))
			{
				$errors[] = _translate('TXT_PASSWORD_INCLUDE_LCL');
			}
			
			if($strenght > 4 && !vTextContent($password,'symbols'))
			{
				$errors[] = _translate('TXT_PASSWORD_INCLUDE_SYMBOL');
			}
			
			if($password !== $confirmation)
			{
				$errors[] = _translate('TXT_PASSWORD_MISSPELLED');
			}
		}
		
		return $errors;
	}
	
	function vUsername($username)
	{
		$errors = getLengthError($username,'username');
		if(empty($errors) && !vTextContent($username,'username'))
			$errors[] = _translate('TXT_INVALID_USERNAME');
		
		return $errors;
	}
	
	function vFirstname($name)
	{
		$errors = getLengthError($name,'firstname');
		if(empty($errors) && !vTextContent($name,'name'))
			$errors[] = _translate('TXT_INVALID_NAME');
			
		return $errors;
	}
	
	function vLastname($name)
	{
		$errors = getLengthError($name,'lastname');
		if(empty($errors) && !vTextContent($name,'name'))
			$errors[] = _translate('TXT_INVALID_NAME');
		
		return $errors;
	}
	
	function vEmail($address)
	{
		$errors = getLengthError($address,'email');
		if(empty($errors)){
		
			if(filter_var($address,FILTER_VALIDATE_EMAIL) === false)
				$errors[] = _translate('TXT_INVALID_EMAIL');
				
			$url = explode("@",$address); 
			if(empty($errors) && !checkdnsrr(end($url)))
				$errors[] = _translate('TXT_INVALID_EMAIL_DOMAIN');
		}
		
		return $errors;
	}
	
	function vURL($address)
	{
		$errors = getLengthError($address,'url');
		if(empty($errors) && filter_var($address,FILTER_VALIDATE_URL) === false)
			$errors[] = _translate('TXT_INVALID_URL');
		
		return $errors;
	}
	
	function vDescription($str)
	{
		$errors = getLengthError($str,'description');
		
		return $errors;
	}
	
	function vPageLinks($page,$total,$maxRows)
	{
			$page = (int) $page;
			$page = max(1,$page);
			
			$maxRows = (int)$maxRows;
			$maxRows = max(1,$maxRows);
			
			$pageNumbers = ceil($total/$maxRows);
			$pageNumbers = max(1,$pageNumbers);

			if($page > $pageNumbers)
				$page = $pageNumbers;
			
			$limit = ($page - 1) * $maxRows;
			
			$end = $maxRows*$page;
			if($end > $total) $end = $total;
			
			return ['page' => $page, 'page_numbers' => $pageNumbers, 'limit' => $limit, 'max_rows' => $maxRows,'end' => $end];
	}
	
	