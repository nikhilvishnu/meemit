<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div id='content_categories' class='responsiveContainer'>
	<h2><?=$g_title?></h2>
	<p><?=_translate('TXT_CATEGORIES')?></p>
	<?php require __DIR__ . '/includes/pagelinks.php'; ?>
	<div class='articleContainer'>
	<?php foreach($articles as $category): ?>
		<article>
			<section class='article'>
			<h3>
				<a href='<?=$g_hostURL.'/category/'.$category['id'].'/'.rawurlencode($category['urlname'])?>'><?=sanitizeSpChars($category['urlname'])?></a>
			</h3>
			<p>
				<?=sanitizeSpChars($category['description'])?>
				<br />
				<span><?=_translate('CONTRIBUTED').' '.sanitizeSpChars($category['contributions'])?></span>
			</p>
			</section>
		</article>
		
	<?php 
		endforeach; 
		if(empty($articles)):
	?>

		<article>
			<section class='article'>
				<h3><?=_translate('EMPTY')?></h3>
				<p><?=_translate('TXT_CATEGORIES_NONE')?></p>
			</section>
		</article>
		
	<?php endif; ?>
	
	</div>
	<?php require __DIR__ . '/includes/pagelinks.php'; ?>
	<?php include __DIR__ . '/includes/ads_footer.php'; ?>
</div>

<div id='content_panel' class='sidePanel'>
	<form id='formSearch' action="<?=$g_hostURL?>/page/categories" method='POST'>
		<div>
			<input type='search' placeholder='submeemits' name="search" id="inputSearch" /> 
		</div>
		<div>
			<button type='submit' name='submitSearch' value='Search'><?=_translate('SEARCH')?></button>
		</div>
	</form>
	<?php include __DIR__ . '/includes/ads_sidebar.php'; ?>
</div>