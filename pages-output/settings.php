<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div class='subContentContainer'>
	<form id='formSettings' action="<?=$g_hostURL?>/page/settings" method='POST'>
		<div id='content_username' class='subContainer'>
			<h2><?=_translate('TIT_MORE_ABOUT')?></h2>
			<?php if($saved): ?>
			
			<p class='successContent'>
				<?=_translate('TXT_PROFILE_SAVED')?>
			</p>
			
			<?php endif; ?>
			<fieldset class='subContent'>
				<legend><?=_translate('PROFILE')?></legend>
				<p class='formDescription'><?=_translate('TXT_DESC_SETTINGS')?></p>
				<div>
					<label for='inputUsername'><?=_translate('USERNAME')?></label>
					<p class='inputDescription<?=$classUsername?>'><?=$descUsername?></p>
					<input name="username" id="inputUsername" pattern='.{<?=LENGTH_MIN_USERNAME?>,}' value='<?=$username?>' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_USERNAME)?>' maxlength="<?=LENGTH_MAX_USERNAME?>" />
				</div>
				<div>
					<label for='inputFirstname'><?=_translate('FIRSTNAME')?></label>
					<p class='inputDescription<?=$classFirstname?>'><?=$descFirstname?></p>
					<input name="firstname" id="inputFirstname" pattern='.{<?=LENGTH_MIN_NAME?>,}' value='<?=$firstname?>' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_NAME)?>' maxlength="<?=LENGTH_MAX_NAME?>" />
				</div>
				<div>
					<label for='inputLastname'><?=_translate('LASTNAME')?></label>
					<p class='inputDescription<?=$classLastname?>'><?=$descLastname?></p>
					<input name="lastname" id="inputLastname" pattern='.{<?=LENGTH_MIN_NAME?>,}' value='<?=$lastname?>' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_NAME)?>' maxlength="<?=LENGTH_MAX_NAME?>" />
				</div>
				<div>
					<label for='textareaDescription'><?=_translate('DESCRIPTION')?></label>
					<p class='inputDescription<?=$classDescription?>'><?=$descDescription?></p>
					<textarea name='description' id='textareaDescription' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_DESCRIPTION)?>'
						placeholder='<?=sprintf(_translate('TXT_MAX_LENGTH'),LENGTH_MAX_DESCRIPTION)?>'
						maxlength="<?=LENGTH_MAX_DESCRIPTION?>"><?=$description?></textarea>
				</div>
				<fieldset class='checkboxfield'>
					<legend><?=_translate('PRIVACY')?></legend>
					<div>
						<input type='checkbox' name='showFirstname' value='1' id='checkboxFirstname'<?=$checkedFirstname?> />
						<label for='checkboxFirstname' class='labelInline'><?=_translate('SHOW_FIRSTNAME')?></label>
					</div>
					<div>
						<input type='checkbox' name='showLastname' value='1' id='checkboxLastname'<?=$checkedLastname?> />
						<label for='checkboxLastname' class='labelInline'><?=_translate('SHOW_LASTNAME')?></label>
					</div>
				</fieldset>
				<div>
					<button type='submit' name='submitChange' value='<?=_translate('CHANGE')?>'><?=_translate('CHANGE')?></button>
				</div>
			</fieldset>
		</div>
		<div id='content_password' class='subContainer'>
			<h2><?=_translate('PASSWORD')?></h2>
			<fieldset class='subContent'>
				<legend><?=_translate('QSN_PASSWORD')?></legend>
				<p class='formDescription'><?=_translate('TXT_DESC_PASSWORD')?></p>
				<div>
					<label for='inputPassword'><?=_translate('PASSWORD')?></label>
					<p class='inputDescription<?=$classPassword?>'><?=$descPassword?></p>
					<input type="password" name="password" id="inputPassword" pattern='.{<?=LENGTH_MIN_PASSWORD?>,}' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_PASSWORD)?>' maxlength="<?=LENGTH_MAX_PASSWORD?>" />
				</div>
				<div>
					<label for='inputPasswordConfirm'><?=_translate('REPEAT_PASSWORD')?></label>
					<input type="password" name="passwordConfirm" id="inputPasswordConfirm" placeholder='<?=_translate('QSN_MISSPELLED')?>' />
				</div>
				<?php if($clientUser->password()): ?>
				
				<div>
					<label for='inputPasswordOld'><?=_translate('OLD_PASSWORD')?></label>
					<p class='inputDescription<?=$classPasswordOld?>'><?=$descPasswordOld?></p>
					<input type="password" name="passwordOld" id="inputPasswordOld" />
				</div>
				
				<?php endif; ?>
				<div>
					<button type='submit' name='submitChange' value='<?=_translate('CHANGE')?>'><?=_translate('CHANGE')?></button>
				</div>
			</fieldset>
		</div>
		<div id='content_openid' class='subContainer'>
			<h2><?=_translate('OPENID')?></h2>
			<fieldset class='subContent'>
				<legend><?=_translate('QSN_OPENID')?></legend>
				<p class='formDescription'><?=_translate('TXT_DESC_OPENID')?></p>
				<div class='openIdLinkContainer'>
					<span class='openIdLink'><?=$openid_google?></span>
					<a id='buttonGoogle' 
						href='<?=$g_hostURL?>/page/openid/provider/www.google.com&amp;login' 
						title='<?=_translate('GOOGLE')?>'><span><?=_translate('GOOGLE')?></span>
					</a>
				</div>
			</fieldset>
		</div>
	</form>
</div>