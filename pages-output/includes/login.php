<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<form id='formLogin' action="<?=$g_hostURL?>/page/authenticate" method='POST'>
	<fieldset class='subContent'>
		<legend><?=_translate('QSN_HAVE_ACCOUNT')?></legend>
		<p class='formDescription'><?=_translate('TXT_DESC_LOGIN')?></p>
		<div>
			<label for='inputLogin'><?=_translate('LOGIN')?></label>
			<input name="login" id="inputLogin" placeholder='Username or Email' value='' required />
		</div>
		<div>
			<label for='inputPasswordLogin'><?=_translate('PASSWORD')?></label>
			<input type="password" name="password" id="inputPasswordLogin" pattern='.{<?=LENGTH_MIN_PASSWORD?>,}' 
				title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_PASSWORD)?>' maxlength="<?=LENGTH_MAX_PASSWORD?>" required />
		</div>
		<div>
			<button type='submit' name='submitLogin' value='<?=_translate('LOGIN')?>'><?=_translate('LOGIN')?></button>
		</div>
		<div class='openIDButtons'>
			<a id='buttonGoogle' href='<?=$g_hostURL?>/page/authenticate/openid/www.google.com&amp;login' title='<?=_translate('LNK_GOOGLE')?>'><span><?=_translate('LNK_GOOGLE')?></span></a>
		</div>
	</fieldset>
</form>