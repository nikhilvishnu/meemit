<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

	$g_title = _translate('FRONT');
	$g_htmlDescription 	.= ' '. _translate('HTML_DESCRIPTION_HOME');
	
	if($g_category->id() > 0)
	{
		$categoryName = $g_category->urlname();
		$categoryName[0] = mb_strtoupper($g_category->urlname()[0]);
		$g_title = $categoryName;
	}
	
	if($g_getSort)
		$g_title .= ' - '.$g_menuSort[$g_getSort];
		
	$pageLink = ifsetor($_REQUEST['page_link']);
	$search = ifsetor($_REQUEST['search'],'');
	
	$arrContributions = $g_contribution->getMany($pageLink,$search,$g_category->id(),$g_getSort,$g_userId);
	$pageVars = $arrContributions[0];
	$articles = $arrContributions[1];
	
	$searched = '';
	if($search)
		$searched = '/search/'.rawurlencode($search);
	$pageURL = $g_sortedURL.$searched.'&amp;page_link=';
	
	$output_pageLinks = pageLinks($pageURL,$pageVars['page_numbers'],$pageVars['page_numbers'],$pageVars['page']);
	
	
