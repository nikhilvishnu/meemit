<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	require __DIR__ . '/configs.php';

	session_start();
	
	require __DIR__ . '/pages.function.php';
	require __DIR__ . '/functions.php';
	require __DIR__ . '/validations.function.php';
	require __DIR__ . '/main.class.php';
	
	$g_ip = getIP(false);
	$g_hostURL = getHostURL();
	
	$session = new Session();
	$site = new Site();
	
	$clientUser 	= $session->checkAuthorized();
	
	$g_userId 		= getClientId();
	$g_authorized 	= authorized();
	
	if($g_authorized && !$clientUser){
		
		$session->destroy();
		headerRedirect($g_hostURL);
	}
	
	setTimezone($g_ip);
	