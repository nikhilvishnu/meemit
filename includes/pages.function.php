<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}

	function page($name)
	{
		if(authorized()){
		
			switch($name)
			{
				case 'contribute': 				$page = 'contribute'; break;
				case 'contribution_delete': 	$page = 'contribution_delete'; break;
				case 'vote': 					$page = 'vote'; break;
				case 'categories': 				$page = 'categories'; break;
				case 'profile': 				$page = 'profile'; break;
				case 'settings': 				$page = 'settings'; break;
				case 'openid': 					$page = 'openid'; break;
				case 'logout': 					$page = 'logout'; break;
				default:						$page = 'home';
			}
		}
		else
		{
			switch($name)
			{
				case 'categories': 			$page = 'categories'; break;
				case 'profile': 			$page = 'profile'; break;
				case 'vote':
				case 'register': 			$page = 'register'; break;
				case 'authenticate': 		$page = 'authenticate'; break;
				default: 					$page = 'home';
			}
		}
		
		return $page;
	}
	
	function pageAjax($name)
	{
		if(authorized()){
		
			switch($name)
			{
				case 'vote': 					$page = 'vote'; break;
				default: 						$page = 'unauthorized';
			}
		}
		else
		{
			switch($name)
			{
				default: 						$page = 'unauthorized';
			}
		}
		
		return $page;
	}
	