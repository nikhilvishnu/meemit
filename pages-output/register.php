<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div class='subContentContainer'>
	<div id='content_register' class='subContainer'>
		<h2><?=_translate('TIT_CREATE_ACCOUNT')?></h2>
		<form id='formRegister' action="<?=$g_hostURL?>/page/register" method='POST'>
			<fieldset class='subContent'>
				<legend><?=_translate('QSN_JOIN')?></legend>
				<p class='formDescription'><?=_translate('TXT_DESC_REGISTER')?></p>
				<div>
					<label for='inputEmail'><?=_translate('EMAIL')?></label>
					<p class='inputDescription<?=$classEmail?>'><?=$descEmail?></p>
					<input type="email" name="email" id="inputEmail" pattern='.{<?=LENGTH_MIN_EMAIL?>,}' value='<?=$email?>' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_EMAIL)?>' maxlength="<?=LENGTH_MAX_EMAIL?>" required />
				</div>
				<div>
					<label for='inputPassword'><?=_translate('PASSWORD')?></label>
					<p class='inputDescription<?=$classPassword?>'><?=$descPassword?></p>
					<input type="password" name="password" id="inputPassword" pattern='.{<?=LENGTH_MIN_PASSWORD?>,}' 
						title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_PASSWORD)?>' maxlength="<?=LENGTH_MAX_PASSWORD?>" required />
				</div>
				<div>
					<label for='inputPassword2'><?=_translate('REPEAT_PASSWORD')?></label>
					<input type="password" name="password2" id="inputPassword2" placeholder='<?=_translate('QSN_MISSPELLED')?>' required />
				</div>
				<div class='si_captcha'>
					<label for='inputCaptcha'><?=_translate('CAPTCHA')?></label>
					<img id="siimage" class="si_image" 
						src="<?=$g_hostURL?>/securimage/securimage_show.php?sid=<?=md5(uniqid())?>" alt="CAPTCHA Image"> 
				</div>
				
				<div>
					<div class='si_option'>
						<object type="application/x-shockwave-flash" class="si_audio" 
							data="<?=$g_hostURL?>/securimage/securimage_play.swf?icon_file=<?=$g_hostURL?>/securimage/images/audio_icon.png&amp;audio_file=<?=$g_hostURL?>/securimage/securimage_play.php">
							
							
							<param name="movie" value="<?=$g_hostURL?>/securimage/securimage_play.swf?icon_file=<?=$g_hostURL?>/securimage/images/audio_icon.png&amp;audio_file=<?=$g_hostURL?>/securimage/securimage_play.php">
						</object>
					</div>
					 
					<div class='si_option'>
						<a tabindex="-1" href="#" title="Refresh Image" 
							onclick="document.getElementById('siimage').src = '<?=$g_hostURL?>/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false">
								<img src="<?=$g_hostURL?>/securimage/images/refresh.png" class='si_refresh' alt="Reload Image">
						</a>
					</div>
				</div>
				
				<div class='si_captchaInput'>
					<label for='inputCaptcha'><?=_translate('ENTER_CODE')?></label>
					<p class='inputDescription<?=$classCaptcha?>'><?=$descCaptcha?></p>
					<input name="captcha" maxlength="16" placeholder='<?=_translate('TXT_CAPTCHA_PLACEHOLDER')?>' id="inputCaptcha" required />
				</div>
				
				<div>
					<button type='submit' name='submitRegister' value='<?=_translate('JOIN')?>'><?=_translate('JOIN')?></button>
				</div>
			</fieldset>
		</form>
	</div>

	<div id='content_login' class='subContainer'>
		<h2><?=_translate('LOGIN')?></h2>
		<?php require __DIR__ . '/includes/login.php'; ?>
	</div>
</div>

