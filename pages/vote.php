<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('VOTE');
	
	$id = ifsetor($_GET['id']);
	$positive = ifsetor($_GET['positive']);
	$positive = (int) $positive;
	
	$clientUser->voteContribution($id,$positive);
	headerRedirect($g_previousURL);
	