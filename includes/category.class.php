<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	class Category extends Main
	{
		private $categoryRows = [];
		
		private $id 			= 0;
		private $urlname 		= '';
		private $description 	= '';
		private $arrange 		= 0;
		private $contributions 	= 0;
		private $comments 		= 0;
		private $created 		= '';
		
		private function ifValueSetProperty(array $values)
		{
			foreach($values as $property => $value)
			{
				if(property_exists($this,$property))
					$this->$property = $value;
			}
		}
		
		function __construct($id = null,$select = '*')
		{
			parent::__construct();
			$this->categoryRows = $this->dbHelper->selectTablesRows(SQL_DB_CATEGORIES);
			$this->getById($id,$select);
		}
		
		public function id(){return $this->id;}
		public function urlname(){return $this->urlname;}
		public function description(){return $this->description;}
		public function arrange(){return $this->arrange;}
		public function contributions(){return $this->contributions;}
		public function comments(){return $this->comments;}
		public function created(){return $this->created;}
		
		public function getById($id,$select)
		{
			$id = (int)$id;
			if($id < 1) return false;
			
			$category = $this->dbHelper->selectCategoryById($id,$select);
			if($category){
			
				$this->ifValueSetProperty($category);
				return $category;
			}
			
			return false;
		}
		
		public function getMenu()
		{
			$limit = 10;
			if($this->categoryRows['rows'] < $limit)
				$limit = $this->categoryRows['rows'];
				
			return $this->dbHelper->selectCategoriesByCount($limit);
		}
		
		public function getMany($page = 1, $search = '', $maxRows = 10)
		{
			$total = $this->categoryRows['rows'];
			$searchCount = $this->dbHelper->countSimpleSearchByName(SQL_DB_CATEGORIES,'urlname',$search);
			if($searchCount !== false)
				$total = $searchCount;
				
			$pages = vPageLinks($page,$total,$maxRows);
			$categories = $this->dbHelper->selectCategorieBySearchLimit($search,$pages['limit'],$pages['max_rows']);
			if($categories)
				return [$pages,$categories];
			
			return [$pages,[]];
		}
	}
	
	