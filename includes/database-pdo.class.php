<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	class DatabaseHandlerPDO extends DatabaseHelper
	{
		function __destruct()
		{
			$this->handler = null;
		}
		
		public function connect()
		{
			$pdo_connection = sprintf('%s:host=%s;port=%d;dbname=%s;charset=%s',
			
				parent::type(),
				parent::host(),
				parent::port(),
				parent::database(),
				parent::charset());
				
			$this->handler = new PDO($pdo_connection, parent::user(), parent::password());
			if($this->handler)
			{
				$this->handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->handler->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$this->connected = true;
				
				return true;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function prepare($sql){return $this->handler->prepare($sql);}
		public function lastInsertId(){return $this->handler->lastInsertId();}
		public function beginTransaction(){return $this->handler->beginTransaction();}
		public function commit(){return $this->handler->commit();}
		public function rollBack(){return $this->handler->rollBack ();}
		
		public function bindParam($stmt,$i,&$value,$type = 's')
		{
			if(!($stmt instanceof PDOStatement))
				return false;
				
			$type = $this->dataType($type);
			return $stmt->bindParam($i,$value,$type);
		}
		
		public function closeStatement($stmt)
		{
			if(!($stmt instanceof PDOStatement))
				return false;
				
			$stmt->closeCursor();
			$stmt = null;
			
			return true;
		}
		
		public function execute($stmt)
		{
			if(!($stmt instanceof PDOStatement))
				return false;
		
			return $stmt->execute();
		}
		
		public function setLastError($stmt = null)
		{
			if(!$this->connected) return false;
			
			$errorInfo = ($stmt instanceof PDOStatement) ? $stmt->errorInfo() : $this->handler->errorInfo();
			if($errorInfo)
			{
				$this->errno = $errorInfo[1];
				$this->error = $errorInfo[2];
			}
			
			return true;
		}
		
		protected function dataType($type)
		{
			switch($type)
			{
				case 'i': $type = PDO::PARAM_INT; break;
				case 's': $type = PDO::PARAM_STR; break;
				case 'd': $type = PDO::PARAM_STR; break;
				case 'l': $type = PDO::PARAM_LOB; break; 
				case 'B': $type = PDO::PARAM_LOB; break; 
				case 'b': $type = PDO::PARAM_BOOL; break; 
				case 'n': $type = PDO::PARAM_NULL; break; 
				default	: $type = -1;
			}
			
			return $type;
		}
		
		public function bindArray($stmt,array &$parameters)
		{
			if(!($stmt instanceof PDOStatement))
				return false;
		
			$params = [];
			for($i = 0; $i < count($parameters); $i++)
			{
				$params[$i] 	= $parameters[$i][0];
				$type 			= $parameters[$i][1];
				$length 		= isset($parameters[$i][2]) ? (int)$parameters[$i][2] 	: 0;
				
				if($params[$i] === null)
				{
					$type = 'n';
				}

				$type = $this->dataType($type);
				
				$bound = null;
				if($length > 0) $bound = $stmt->bindValue($i + 1,$params[$i],$type,$length);
				else 			$bound = $stmt->bindValue($i + 1,$params[$i],$type);
				
				if(!$bound)
				{
					$this->setLastError($stmt);
					return false;
				}
			}
			
			return true;
		}
		
		public function executeQuery($sql,array $params = [])
		{
			if(!$this->connected) return false;
		
			$stmt = $this->handler->prepare($sql);
			if($stmt){

				$affectedRows 	= 0;
				$executed 		= false;
				
				$bound = $this->bindArray($stmt,$params);
				if($bound)
				{
					$executed = $stmt->execute();
					if($executed)
						$affectedRows = $stmt->rowCount();
				}
				
				$error = false;
				if(!$bound || !$executed)
					$error = $this->setLastError($stmt);
				
				$stmt->closeCursor();
				$stmt = null;
				
				if($error)
					return false;
				
				return $affectedRows;
			}

			$this->setLastError();
			return false;
		}
		
		public function fetchAssoc($sql,array $params = [],$all = false)
		{
			if(!$this->connected) return false;
		
			$stmt = $this->handler->prepare($sql);
			if($stmt){

				$rows 		= [];
				$executed 	= false;

				$bound = $this->bindArray($stmt,$params);
				if($bound)
				{
					$executed = $stmt->execute();
					if($executed)
						$rows = ($all) ? $stmt->fetchAll(PDO::FETCH_ASSOC) : $stmt->fetch(PDO::FETCH_ASSOC);
				}
				
				$error = false;
				if(!$bound || !$executed)
					$error = $this->setLastError($stmt);
					
				$stmt->closeCursor();
				$stmt = null;
				
				if($error)
					return false;
				
				return $rows;
			}

			$this->setLastError();
			return false;
		}
	}
	
	