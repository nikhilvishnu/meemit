<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	//xMySQLi by Guido
	//http://jp.php.net/manual/en/mysqli-stmt.bind-param.php#110363
	class xMySQLi extends mysqli
	{
		public function prepare($query){return new xMySQLi_stmt($this,$query);}
	}

	class xMySQLi_stmt extends mysqli_stmt
	{
		private $mbind_types = array();
		private $mbind_params = array();
	
		public function __construct($link, $query)
		{
			$this->mbind_reset();
			parent::__construct($link, $query);
		}

		public function mbind_reset()
		{
			unset($this->mbind_params);
			unset($this->mbind_types);
			
			$this->mbind_params = array();
			$this->mbind_types = array();
		}
	   
		//use this one to bind params by reference
		public function mbind_param($type, &$param)
		{
			if(empty($this->mbind_types[0]))
				$this->mbind_types[0] = '';
		
			$this->mbind_types[0] .= $type;
			$this->mbind_params[] = &$param;
		}
	   
		//use this one to bin value directly, can be mixed with mbind_param()
		public function mbind_value($type, $param)
		{
			if(empty($this->mbind_types[0]))
				$this->mbind_types[0] = '';
				
			$this->mbind_types[0] .= $type;
			$this->mbind_params[] = $param;
		}
		
		public function mbind_param_do()
		{
			$params = array_merge($this->mbind_types, $this->mbind_params);
			return call_user_func_array(array($this, 'bind_param'), $this->makeValuesReferenced($params));
		}
	   
		private function makeValuesReferenced($arr)
		{
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			
			return $refs;
		}
	   
		public function execute()
		{
			if(count($this->mbind_params))
				$this->mbind_param_do();
			   
			return parent::execute();
		}
	}
	
	//END
	//-------------------------------------------------------------------------------------------------------------------------------------------
	
	class DatabaseHandlerMySQLi extends DatabaseHelper
	{
		function __destruct()
		{
			$this->handler->close();
		}
		
		public function connect()
		{
			$driver = new mysqli_driver();
			$driver->report_mode = MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT;
		
			$this->handler = new xMySQLi(parent::host(), parent::user(), parent::password(), parent::database(), parent::port());
			if($this->handler)
			{
				$this->handler->autocommit(true);
				$this->handler->set_charset(parent::charset());
				$this->connected = true;
				
				return true;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function prepare($sql){return $this->handler->prepare($sql);}
		public function lastInsertId(){return $this->handler->insert_id;}
		public function beginTransaction(){return $this->handler->autocommit(false);}
		
		public function commit()
		{
			$commit = $this->handler->commit();
			
			$this->handler->autocommit(true);
			return $commit;
		}
		
		public function rollBack()
		{
			$rollback = $this->handler->rollback();
			
			$this->handler->autocommit(true);
			return $rollback;
		}
		
		public function bindParam($stmt,$i,&$value,$type = 's')
		{
			if(!($stmt instanceof mysqli_stmt))
				return false;
				
			$type = $this->dataType($type);
			return $stmt->mbind_param($type,$value);
		}
		
		public function closeStatement($stmt)
		{
			if(!($stmt instanceof mysqli_stmt))
				return false;
				
			$stmt->close();

			return true;
		}
		
		public function execute($stmt)
		{
			if(!($stmt instanceof mysqli_stmt))
				return false;
		
			return $stmt->execute();
		}
		
		public function setLastError($stmt = null)
		{
			if(!$this->connected) return false;
			
			if($stmt instanceof mysqli_stmt)
			{
				$this->errno = $stmt->errno;
				$this->error = $stmt->error;
				
				return true;
			}

			$this->errno = $this->handler->errno;
			$this->error = $this->handler->error;
			
			return true;
		}
		
		protected function dataType($type)
		{
			switch($type)
			{
				case 'i': $type = 'i'; break;
				case 's': $type = 's'; break;
				case 'd': $type = 'd'; break;
				case 'l': $type = 's'; break; 
				case 'B': $type = 'b'; break; 
				case 'b': $type = 'i'; break; 
				default	: $type = 's';
			}
			
			return $type;
		}
		
		public function bindArray($stmt,array &$parameters)
		{
			if(!($stmt instanceof mysqli_stmt))
				return false;
				
			if(count($parameters) == 0)
				return true;
		
			$params = [];
			$type = '';
			for($i = 0; $i < count($parameters); $i++)
			{
				$params[$i] 	= &$parameters[$i][0];
				$type 			.= $this->dataType($parameters[$i][1]);
			}
			
			if(call_user_func_array(array(&$stmt, "bind_param"),array_merge([$type],$params)))
			{
				return true;
			}
			
			$this->setLastError($stmt);
			return false;
		}
		
		public function executeQuery($sql,array $params = [])
		{
			if(!$this->connected) return false;
		
			$stmt = $this->handler->prepare($sql);
			if($stmt){

				$affectedRows 	= 0;
				$executed 		= false;
				
				$bound = $this->bindArray($stmt,$params);
				if($bound)
				{
					$executed = $stmt->execute();
					if($executed)
						$affectedRows = $stmt->affected_rows;
				}
				
				$error = false;
				if(!$bound || !$executed)
					$error = $this->setLastError($stmt);
				
				$stmt->close();
				
				if($error)
					return false;
				
				return $affectedRows;
			}

			$this->setLastError();
			return false;
		}
		
		public function fetchAssoc($sql,array $params = [],$all = false)
		{
			if(!$this->connected) return false;
		
			$stmt = $this->handler->prepare($sql);
			if($stmt){

				$rows 		= [];
				$executed 	= false;

				$bound = $this->bindArray($stmt,$params);
				if($bound)
				{
					$executed = $stmt->execute();
					if($executed)
					{
						$result = $stmt->get_result();
						if($result)
						{
							$rows = ($all) ? $result->fetch_all(MYSQLI_ASSOC) : $result->fetch_assoc();
							$result->free_result();
						}
					}
				}
				
				$error = false;
				if(!$bound || !$executed)
					$error = $this->setLastError($stmt);
				
				$stmt->close();
				
				if($error)
					return false;
					
				return $rows;
			}
			
			$this->setLastError();
			return false;
		}
	}
	
	