<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

?>

<div id='content_posts' class='responsiveContainer'>

	<h2><?=_translate('CONTRIBUTIONS')?></h2>
	<p>...</p>

	<?php require __DIR__ . '/includes/pagelinks.php'; ?>
	<div class='articleContainer'>
		
		<?php require __DIR__ . '/includes/contributions.php'; ?>
		
	</div>
	<?php require __DIR__ . '/includes/pagelinks.php'; ?>
	<?php include __DIR__ . '/includes/ads_footer.php'; ?>
</div>
<div id='content_home' class='sidePanel'>
	<form id='formSearch' action="<?=$g_hostURL.$g_sortedURL?>" method='POST'>
		<div>
			<input type='search' placeholder='Meemits' name="search" id="inputSearch" /> 
		</div>
		<div>
			<button type='submit' name='submitSearch' value='Search'><?=_translate('SEARCH')?></button>
		</div>
	</form>
	<?php if(!$g_authorized): ?>
	<div id='loginBox'>
		<form id='formLoginHome' action="<?=$g_hostURL?>/page/authenticate" method='POST'>
			<div>
				<input placeholder='<?=_translate('LGN_USERNAME_EMAIL')?>' name="login" id="inputLogin" required /> 
				<input type="password" placeholder='Password' name="password" id="inputPassword" pattern='.{<?=LENGTH_MIN_PASSWORD?>,}' 
					title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_PASSWORD)?>' maxlength="<?=LENGTH_MAX_PASSWORD?>" required />
			</div>
			<div>
				<button type='submit' name='submitLogin' value='Login'><?=_translate('LOGIN')?></button>
			</div>
		</form>
		<div class='openIDButtons'>
			<a id='buttonGoogle' href='<?=$g_hostURL?>/page/authenticate/openid/www.google.com&amp;login' title='<?=_translate('LNK_GOOGLE')?>'>
				<span><?=_translate('LNK_GOOGLE')?></span>
			</a>
		</div>
	</div>
	<?php if(!$g_category->description()): ?>
	<div class='descriptionContent'>
		<h2><?=_translate('QSN_MEEMIT')?></h2>
		<p><?=_translate('TXT_DESC_MEEMIT')?></p>
	</div>
	<?php endif; else: ?>
	
	<div id='meemitButtons'>
		<a href='<?=$g_hostURL?>/page/contribute/<?=$g_category->id()?>'><?=_translate('LNK_SUBMIT_LINK')?></a>
	</div>
	
	<?php endif; if($g_category->description()): ?>

	<div class='descriptionContent'>
		<h2><?=sanitizeSpChars($g_category->urlname())?></h2>
		<p><?=sanitizeSpChars($g_category->description())?></p>
	</div>
	
	<?php endif; ?>
	
	<?php include __DIR__ . '/includes/ads_sidebar.php'; ?>
	
</div>