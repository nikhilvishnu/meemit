<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div class='subContentContainer'>
	<div id='content_openid' class='subContainer'>
		<h2><?=_translate('OPENID')?></h2>
		
		<?php if($errors): ?>
		<p class='failureContent'>
			<?=$errors[0]?>
			<br />
			<a href='<?=$g_hostURL. '/page/settings'?>'>Go Back</a>
		</p>
		<?php endif; ?>
		
	</div>
</div>