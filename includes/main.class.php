<?php

	if(!IN_SITE)
	{
		exit;
	}
	
	require_once __DIR__ . '/configs.class.php';
	require_once __DIR__ . '/database-handler.class.php';
	
	class Main
	{
		private 	$db;
		protected	$dbHelper;
		
		private $errorMessages = [];
	   
		public function __construct()
		{
			DatabaseFactory::create(Configs::getDatabaseExtension());

			$this->dbHelper = DatabaseFactory::getInstance();
			$this->db = $this->dbHelper->handler();
		}
		
		public function getErrorMessages($index = null)
		{
			if($index){
			
				$msg = isset($this->errorMessages[$index]) ? $this->errorMessages[$index] : '';
				return $msg;
			}
			
			return $this->errorMessages;
		}
		
		public function getFirstError($index = null)
		{
			if($index && isset($this->errorMessages[$index]))
				return $this->errorMessages[$index][0];
			
			if(isset($this->errorMessages[0]))
				return $this->errorMessages[0];
				
			return '';
		}
		
		public function getErrorAllMessages()
		{
			$errors = [];
			foreach($this->errorMessages as $error)
			{
				if(is_array($error)){
				
					foreach($error as $suberror)
					{
						$errors[] = $suberror;
					}
					
					continue;
				}
				
				$errors[] = $error;
			}
		
			return $errors;
		}
		
		public function setErrorMessages($msg,$index = null)
		{
			if(is_array($msg)){
			
				$merged = array_merge($this->errorMessages,$msg);
				if($merged){
				
					if($index) $this->errorMessages[$index] = $merged;
					else $this->errorMessages = $merged;
					
					return true;
				}
				
				return false;
			}
			
			if($index) $this->errorMessages[$index][] = $msg;
			else $this->errorMessages[] = $msg;
			
			return true;
		}
		
		protected function db(){return $this->db;}
	}
	
	require_once __DIR__ . '/session.class.php';
	require_once __DIR__ . '/user.class.php';
	require_once __DIR__ . '/site.class.php';
	
	