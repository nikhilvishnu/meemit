<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$g_title = _translate('LNK_SUBMIT_LINK');
	
	$title = '';
	$url = '';
	$category = $g_category->urlname();
	
	$descTitle 		= '';
	$descURL 		= '';
	$descCategory 	= '';
	$descCaptcha 	= '';
	
	$classTitle		= '';
	$classURL		= '';
	$classCategory	= '';
	$classCaptcha	= '';
	
	$catId = ifsetor($_GET['id']);
	
	$submitContribution = ifsetor($_POST['submitContribution']);
	if($submitContribution){
		$title = ifsetor($_POST['title']);
		$url = ifsetor($_POST['url']);
		$category = ifsetor($_POST['category'],$g_category->urlname());
		$captcha = ifsetor($_POST['captcha']);
		
		$contributed = $g_contribution->create($clientUser,$title,$url,$category,$captcha);
		if($contributed)
		{
			$_SESSION[SESS_KEY_MENUSORT] = 'new';
			headerRedirect($g_hostURL.'/page/'.$clientUser->getProfileLink());
		}
		
		$error = $site->getFirstError('title');
		if($error) $classTitle = ' failure';
		$descTitle = ($error) ? $error : $descTitle;
		
		$error = $site->getFirstError('url');
		if($error) $classURL = ' failure';
		$descURL = ($error) ? $error : $descURL;
		
		$error = $site->getFirstError('category');
		if($error) $classCategory = ' failure';
		$descCategory = ($error) ? $error : $descCategory;
		
		$error = $site->getFirstError('captcha');
		if($error) $classCaptcha = ' failure';
		$descCaptcha = ($error) ? $error : $descCaptcha;
		
		$title = sanitizeSpChars($title);
		$url = sanitizeSpChars($url);
		$category = sanitizeSpChars($category);
	}
	
	