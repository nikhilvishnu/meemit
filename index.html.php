<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	$output_categories = '';
	foreach($g_category->getMenu() as $g_mCategory){
	
		$g_menuLinkSelected = '';
		if($g_getCategory == $g_mCategory['id'])
			$g_menuLinkSelected = ' class="mLinkSelected"';
		
		$g_url = $g_categoryURL.'/category/'.$g_mCategory['id'].'/'.rawurlencode($g_mCategory['urlname']);
		
		$output_categories .= sprintf('<li><a href="%s" title="%s"%s>%s</a></li>',
			$g_url,
			sanitizeSpChars($g_mCategory['description']),
			$g_menuLinkSelected,
			sanitizeSpChars($g_mCategory['urlname']));
	}
	
	$output_menu = '';
	foreach($g_menuSort as $g_sort => $g_sortName){
	
		$g_menuLinkSelected = '';
		if($g_getSort == $g_sort )
			$g_menuLinkSelected = ' class="mLinkSelected"';
		
		$g_url = $g_sortURL.'/sort/'.$g_sort;
		if($g_category->id() > 0)
			$g_url = $g_sortURL.'/category/'.$g_category->id().'/'.rawurlencode($g_category->urlname()).'/sort/'.rawurlencode($g_sort);
		
		$output_menu .= sprintf('<li><a href="%1$s" title="%2$s"%3$s>%2$s</a></li>',$g_url,sanitizeSpChars($g_sortName),$g_menuLinkSelected);
	}
	
?>

<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title><?=sanitizeSpChars($g_title) .' - '. DOCUMENT_TITLE?></title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="keywords" content="<?=$g_htmlKeywords?>" />
		<meta name="description" content="<?=$g_htmlDescription?>" />
			
		<meta name="author" content="Josh" />
		<meta name="generator" content="HTML,Javascript,PHP" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/main.min.css" />
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/ionicons.min.css" />
		<link rel='icon' type='image/vnd.microsoft.icon' href='<?=$g_hostURL?>/favicon.ico' />
		
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?=$g_hostURL?>/javascripts/respond.min.js"></script>
		<![endif]-->
		
		<?php require __DIR__ . '/includes/thirdparties/analyticstracking.php'; ?>
	</head>
	<body>
		<div id='container'>
			<div id='content_top'>
				<nav id='categorymenu'>
					<ul>
						<li><a href="<?=$g_hostURL?>"><?=_translate('FRONT','LC')?></a></li>
						<li><span class='catSeperator'>|</span></li>
						<?=$output_categories?>
					</ul>
					<div id='categoryMore'><a href='<?=$g_hostURL?>/page/categories'><?=_translate('MORE','LC')?></a></div>
				</nav>
				<header id='header'>
					<div id='header_title'>
						<a href='<?=$g_hostURL?>'>
							<h1><?=DOCUMENT_TITLE?></h1>
							<p><?=_translate('TXT_SLOGAN')?></p>
						</a>
					</div>
					<div id='header_userMenu'>
					<?php if($g_authorized):?>
					
						<div>
							<a href='<?=$g_hostURL?>/page/settings'><?=_translate('SETTINGS')?></a> | 
							<a href='<?=$g_hostURL?>/page/logout'><?=_translate('LOGOUT')?></a>
						</div>
						<div>
							<a 
								title='<?=sanitizeSpChars(mb_strtolower($clientUser->email()))?>' 
								href='<?=$g_hostURL?>/page/<?=$clientUser->getProfileLink()?>'>
									<?=sanitizeSpChars($clientUser->getProfileName())?>
							</a>
						</div>

					
					<?php else:?>
					
						<?=sprintf(_translate('LNK_LOGIN_REGISTER'),$g_hostURL.'/page/register')?>
					
					<?php endif;?>
					</div>
					<div id='header_sortMenu'>
						<div id='catTitle'>
							<a href='#' title='<?=sanitizeSpChars($g_category->description())?>'>
								/<?=mb_strtoupper(sanitizeSpChars($g_category->urlname()))?>
							</a>
						</div>
						<nav id='sortMenu'>
							<ul><?=$output_menu?></ul>
						</nav>
					</div>
				</header>
			</div>
			
			<div id='content'>
				<?php require __DIR__ . '/pages-output/' . $g_getPage . '.php'; ?>
			</div>
			
			<div id='footer_separator'></div>
			
			<footer id='footer'>
				<div id='footer_left'>
				<?php 
				
					foreach($g_menuSort as $g_sort => $g_sortName){
					
						$g_url = $g_sortURL.'/sort/'.$g_sort;
						if($g_category->id() > 0)
							$g_url = $g_sortURL.'/category/'.$g_category->id().'/'.rawurlencode($g_category->urlname()).'/sort/'.rawurlencode($g_sort);
						
						printf('<a href="%1$s" title="%2$s">%2$s</a> ',$g_url,sanitizeSpChars($g_sortName));
					}
					
				?>
				</div>
				<div id='footer_right'>
					<?=DOCUMENT_TITLE?> 2014 Josh
					<br />
					<a href='http://www.xuniver.se/' target='_blank'>Xuniverse</a> 
					<a href='http://www.xuniver.se/page/projects#meemit' target='_blank'>Source</a>
				</div>
			</footer>
		</div>
		<script src="<?=$g_hostURL?>/javascript.php"></script>
	</body>
</html>

