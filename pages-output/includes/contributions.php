<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

?>

<?php foreach($articles as $contribution): ?>

	<?php
	
		$url = parse_url($contribution['url']);
		
		$url_domain = sanitizeSpChars($url['scheme'].'://'.$url['host']);
		$host = sanitizeSpChars($url['host']);
		
		$username = null;
		if(isset($contribution['uid']))
		{
			$url_profile = $g_hostURL.'/page/profile/'.$contribution['uid'];
			$username = _translate('USER').$contribution['uid'];
			if($contribution['username']){
			
				$url_profile .= '/'.rawurlencode($contribution['username']);
				$username = sanitizeSpChars($contribution['username']);
			}
		}
		
		$deleteArticle = '';
		
		$vote_positive = '<a href="'.$g_hostURL.'/page/vote/'.$contribution['id'].'/positive" rel="nofollow"
			data-vote-positive="1" 
			data-vote="'.$contribution['id'].'">%s</a>';
		
		$vote_negative = '<a href="'.$g_hostURL.'/page/vote/'.$contribution['id'].'/negative" rel="nofollow"
			data-vote-positive="0"  
			data-vote="'.$contribution['id'].'">%s</a>';
		
		if($g_userId === $contribution['id_user'])
		{
			$vote_positive = '%s';
			$vote_negative = '%s';
			
			$deleteArticle = '
				<a href="'.$g_hostURL.'/page/delete-contribution/'.$contribution['id'].'" rel="nofollow">
					<i title="'._translate('DELETE').'" class="ion-ios7-trash"></i>
				</a>';
		}
		
		$vote_positive = sprintf($vote_positive,'<i title="'._translate('VOTE_UP').'" class="ion-arrow-up-a"></i>');
		$vote_negative = sprintf($vote_negative,'<i title="'._translate('VOTE_DOWN').'" class="ion-arrow-down-a"></i>');
		
		$voted = ifsetor($contribution['voted']);
		
		$score = '<i class="ion-record articleVoteIcon"></i>';
		if($contribution['votes'] !== 0)
			$score = $contribution['score'];

		$classVotePositive = ($voted === 1) ? ' class="votePositive"' : '';
		$classVoteNegative = ($voted === -1) ? ' class="voteNegative"' : '';

	?>
	
	<article id='articleContribution_<?=$contribution['id']?>'>
		<div class='articleOptions'>
			<?=$deleteArticle?>
		</div>
		<section class='articleVote'>
			<ul>
				<li <?=$classVotePositive?>><?=$vote_positive?></li>
				<li data-voted="<?=(int)$voted?>" data-vote-score="<?=$contribution['score']?>"><?=$score?></li>
				<li <?=$classVoteNegative?>><?=$vote_negative?></li>
			</ul>
		</section>
		<section class='article'>
			<h3>
				<a href='<?=sanitizeSpChars($contribution['url'])?>'><?=sanitizeSpChars($contribution['title'])?></a>
				<span>(<a href='<?=$url_domain?>'><?=$host?></a>)</span>
			</h3>
			<p>
				<?=date(DATE_FORMAT,$contribution['utime'])?>
				<?php if($username): ?> <a href='<?=$url_profile?>'><?=$username?></a><?php endif; ?>
				
				<?php if(isset($contribution['cid'])): ?>
				
				<br />
				<a href='<?=$g_hostURL.'/category/'.$contribution['cid'].'/'.rawurlencode($contribution['category'])?>'><?=sanitizeSpChars($contribution['category'])?></a>
				
				<?php endif; ?>
			</p>
		</section>
	</article>
	
<?php 
	endforeach; 
	if(empty($articles)):
?>

	<article>
		<section class='article'>
			<h3><?=_translate('EMPTY')?></h3>
			<p><?=_translate('TXT_CONTRIBUTIONS_NONE')?></p>
		</section>
	</article>
	
<?php endif; ?>
